#!/bin/sh

installDir=$HOME/bin
echo "This will install gofetch in your $HOME/bin directory"
echo "Installing RMySQL package"
echo 'install.packages("RMySQL", repos="http://cran.r-project.org" );' > temp
#Rscript temp

if [ ! -d $installDir ]; then
	echo "Install directory does not exist >> $installDir"
	echo "Creating one!"
fi

rscriptDir=`which Rscript`
if [ -e $rscriptDir ]; then
	echo "#!$rscriptDir" > temp
	cat temp src/gofetch_source > $installDir/gofetch
	chmod 755 $installDir/gofetch
else 
	echo "Seems like R is not installed. Please install R and add Rscript to your \$PATH environment variable"
	exit 0
fi

echo "Add this line to your ~/.bashrc or ~/.bash_profile. And source it."
echo "PATH=$installDir/:\$PATH"
echo "Cleaning up"
rm -f temp
